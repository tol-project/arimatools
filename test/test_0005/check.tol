Set generate = If(ObjectExist("Set","difOpt"), Copy(Empty), 
  IncludeText(ReadFile("../generateArima.tol")));

Real If(!ObjectExist("Real","iter"), iter=0);
Real If(!ObjectExist("Real","degreeScale"), degreeScale=3);
Text If(!ObjectExist("Text","ols.method"), ols.method="CHO");

Text IdentifyMethod = "I-Almagro & ARMA-Reversible Jump";

Real t0.unitroot = Copy(Time);
WriteLn("Searching for unitary polynomials ...");
Set difProb = ArimaTools::UnitRoot::EvalDifProb(
 z, difOpt, maxPeriod*degreeScale, ols.method, 1000, 0.001);
WriteLn("\nThere are "<<Card(difProb)+" posible unitary polynomials.");
Real elapsed.unitroot = Copy(Time)- t0.unitroot;
Real difProb.num = Card(difProb);
Set difProb.Real.Sel = Select(difProb,Real(Set s)
{
  s::dif==dif
});
Real difProb.Real = If(Card(difProb.Real.Sel)==1,(difProb.Real.Sel[1])::prob,0);

WriteLn("\nFound "<<Card(difProb)+" posible unitary polynomials in "<<elapsed.unitroot+" seconds");


Real t0.arma = Copy(Time);

Set arimaDegProb = For(1,Card(difProb),NameBlock(Real difPos)
{
  Set difDeg = gdo::degree[difProb[difPos][1] ];
  Real difDegProb = difProb[difPos][2];
  WriteLn("Searching ARMA structure conditioned to delta = "<<
    difProb[difPos][3]+" which probability is "<<difDegProb);
  Set arimaDegreeRange = For(1,periodNum,@ARIMADegreeRangeStruct(Real k)
  {
    Set dr = (rndArima::_.degreeRange)[k];
    @ARIMADegreeRangeStruct(
      dr->Periodicity,
      dr->MinARDegree,
      dr->MaxARDegree,
      dr->MinMADegree,
      dr->MaxMADegree,
      difDeg[k],
      difDeg[k]
    ) 
  });
  NameBlock sampler = ArimaTools::@ARIMA.Sampler::CreateFromDegreeRange(
    Matrix rndArimaSer::noise, arimaDegreeRange);
  Real sampler::config::Evaluator:=
    ArimaTools::@ARMA.Sampler::Options::Evaluator::
    Levinson;
  //FastChol;
  //FastCholSea;
  //FactorACF;
  Real sampler::config::CandidateGen:=
    ArimaTools::@PolDeg.Sampler::Options::CandidateGen::
  //MoveAll;
  //MoveOne;
    MoveOneForced;
  Real sampler::set.initialPoint(sampler::get.initialPoint(?));
  //Real TolOprProfiler.Enabled := True;
  Real sampler::build_mcmc(mh.sampleLength);
  //Real TolOprProfiler.Dump("ACF.01")
  sampler
});

Set ranking = 
{
  Set aux1 = SetConcat(For(1,Card(difProb),Set(Real difPos_)
  {
    Real difPos = difPos_;
    Set difDeg = gdo::degree[difProb[difPos][1] ];
    Real difDegProb = difProb[difPos][2];
    EvalSet((arimaDegProb[difPos])::_.ranking,Set(Set s)
    {
      Real prob = s[1]*difDegProb;
      [[prob]] << ExtractByIndex(s,Range(2,Card(s),1)) << [[difPos]]
    })
  }));
  Set aux2 = Select(aux1,Real(Set reg)
  {
    reg::prob>=minProb
  });
  Set aux3 = Sort(aux2,Real(Set a, Set b)
  {
    Compare(b::prob,a::prob)
  })
};

Set degree.real = {
 Set aux.1 = SetConcat(EvalSet(rndArimaSer::arima,Set(@ARIMAStruct factor)
 {[[
   Degree(factor->AR)/factor->Periodicity,
   Degree(factor->MA)/factor->Periodicity,
   Degree(factor->DIF)/factor->Periodicity
 ]]}));
 Set aux.2 = For(1,Card(aux.1),Real(Real k)
 {
   Text name = ((arimaDegProb[1])::_.varName)[k];
   Eval(name+"=aux.1[k]")
 });
 [[ [[ Real {Prob = ?} ]]<<aux.2<<[[Text name="RealModel", Text label]] ]]
};
Set degree.cmp = degree.real << ranking;

Real RNK = Min(maxOptions, Card(ranking));

Real elapsed.arma = Copy(Time)- t0.arma;

Set armaOpt = For(1,RNK,NameBlock(Real rnkPos)
{
  Text name = (ranking[rnkPos])::name;
  ((arimaDegProb[(ranking[rnkPos])::difPos])::_.armaSampler)[name]
});

Set identify.quality = IdentificationQuality(armaOpt);

Set report = [[rndSeed, IdentifyMethod]] << NameBlockToSet((arimaDegProb[1])::config) <<
[[  
  minProb, 
  maxOptions, 
  N,
  difProb.num, 
  difProb.Real, 
  logLikelihood, 
  identify.quality::logLikelihood.max,
  stdev, 
  identify.quality::stdev.min, 
  identify.quality::acor.std, 
  identify.quality::acor.std.min,
  identify.quality::quality.stdev,
  identify.quality::quality.acor,
  identify.quality::quality.llh,
  identify.quality::quality, 
  elapsed.unitroot, 
  elapsed.arma, 
  identify.quality::elapsed.estimation
]];


Set View(report,"Std");
WriteLn("");

/**/

