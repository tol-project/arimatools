////////////////////////////////////////////////////////////////////////////
Serie FillXArima(
  Serie ser, 
  Set drivers, 
  Real lags_, 
  Real box_cox_lambda,
  Real box_cox_delta)
////////////////////////////////////////////////////////////////////////////
{  
  Set bc = @BoxCoxStruct(box_cox_lambda,box_cox_delta);
  Real lags = Case(
    IsUnknown(lags_),2,
    lags_<=1, 2,
    True, lags_);
  Polyn pol = Expand((1-0.1*B^lags)/(1-0.1*B),lags);
  Date firstEstim = Succ(
    SetMaxDate(EvalSet(drivers,First)),
    Dating(ser), lags);
  Date lastEstim  = SetMinDate(EvalSet(drivers,Last ));

  Set modelDef = ModelDef(
  ser,
  box_cox_lambda,  //box-cox transform exponent (0->logarithm)
  box_cox_delta,  //box-cox transform traslation (0->sin traslación)
  1,  //periodicity
  0,  //constant in residuals side (obsolete)
  1,  //difference
  SetOfPolyn(pol,1), //AR factors
  SetOfPolyn(pol,1), //MA factors
  Set inputs = EvalSet(drivers,@InputDef(Serie drv)
  {
    @InputDef(pol,BoxCoxTransf(drv,bc))
  }),  
  Empty); //non linear inputs obsolete
  Set modelEst = Estimate(modelDef,firstEstim,lastEstim);
  Serie full = BoxCoxInvTransf(modelEst::Series::FullTransformed,bc);
  Serie fill = ser << full >> ser
};  
Code PutDescription("Fill uncomplete series using related drivers and automatic ARIMA models",
  FillXArima);