#Require ArimaTools;

//////////////////////////////////////////////////////////////////////////////
//Carga de datos
//////////////////////////////////////////////////////////////////////////////
//Nuestras series ya est�n en una m�trica homoced�stica
// GDP: tasa de variaci�n anual del producto interior bruto
// UNM: porcentaje de desempleados en la poblaci�n activa
// POP: tasa de variaci�n anual del n�mero de habitantes
Set If(ObjectExist("Set","MacroEconomicSeries"), MacroEconomicSeries,
        MacroEconomicSeries = { Include("MacroEconomicSeries.oza") });
Real If(ObjectExist("Real","concept"), concept, concept= 1); //GDP
Real If(ObjectExist("Real","country"), country, country=12); //ESP

Serie output = MacroEconomicSeries[concept][country];
Text name = Name(MacroEconomicSeries[concept][country]);

WriteLn("");
WriteLn("////////////////////////////////////////////////////////////");
WriteLn("Identificacndo modelo ARIMA para la serie "<<name);
WriteLn("////////////////////////////////////////////////////////////");
WriteLn("");

//////////////////////////////////////////////////////////////////////////////
//Estructuras de los rangos de grados ARIMA para cada periodicidad.
//En este caso s�lo hay factor regular con periodo 1
//////////////////////////////////////////////////////////////////////////////
Set ARIMADegRange = { [[
 @ARIMADegreeRangeStruct(
  Period=1,MinAR=0,MaxAR=5,MinMA=0,MaxMA=5,MinDIF=0,MaxDIF=2 ) 
]] };

//////////////////////////////////////////////////////////////////////////////
//Filtro lineal y de outliers
//////////////////////////////////////////////////////////////////////////////
//Configuraci�n por defecto
NameBlock flt.cfg = ArimaTools::Filter::@Config::Default(?);
//Adaptaci�n de la configuraci�n al caso particular
Real flt.cfg::optMaxOrder := 2;
//En este ejercicio no propondremos inputs de filtrado lineal al identificador
Set InputTry = Copy(Empty);
//Filtrado de inputs lineales y AIA
NameBlock filter = ArimaTools::Filter::OverDifAIALinear(
  Serie output,        //Serie output a modelar
  Set ARIMADegRange,   //Rangos de grados ARIMA
  Set InputTry,        //Inputs tentativos
  NameBlock flt.cfg);  //Configuraci�n

//////////////////////////////////////////////////////////////////////////////
//Identificador de ra�ces unitarias.
//////////////////////////////////////////////////////////////////////////////
//Configuraci�n por defecto
NameBlock ur.cfg = ArimaTools::UnitRoot::@Config::Default(?);
//Adaptaci�n de la configuraci�n al caso particular
//N�mero de simulaciones por polinonio propuesto
Real ur.cfg::sampleLength := 1000; 
//Se descartan los polinomios con menos del 0.01% de probabilidad
Real ur.cfg::minProb := 1E-4; 
//Identificador de ra�ces unitarias que construye un ranking ordenado seg�n 
//la probabilidad de cada una de las opciones.
NameBlock id.ur = ArimaTools::UnitRoot::Identify(
  filter::_.filtered, //Serie filtrada de efectos ex�genos
  ARIMADegRange,      //Rangos de grados ARIMA
  ur.cfg);            //Configuraci�n

//////////////////////////////////////////////////////////////////////////////
//Identificador de polinomios ARMA
//////////////////////////////////////////////////////////////////////////////
//Configuraci�n por defecto
NameBlock arma.cfg = ArimaTools::@ARMA.Sampler.ACF.Reg.Config::Default(?);
//Adaptaci�n de la configuraci�n al caso particular
Text arma.cfg::sampler := ArimaTools::@ARMA.Sampler.ACF.Reg.Config::Options::
  Sampler::PriorSampling;
Text arma.cfg::acfCov := ArimaTools::@ARMA.Sampler.ACF.Reg.Config::Options::
  ACFCov::CommutedBarlett;
Text arma.cfg::priorType := ArimaTools::@ARMA.Sampler.ACF.Reg.Config::Options::
  PriorType::NonInf;
Real arma.cfg::acfUseLag := 8;
Real arma.cfg::sampleLength := 1000;
Real arma.cfg::optMaxTime := 0.1;


//Identificador de ra�ces ARMA que construye un ranking ordenado seg�n 
//la probabilidad de cada una de las opciones.
NameBlock id.arma = ArimaTools::@ARMA.Splitter.ACF::Identify(
  filter::_.filtered, //Serie filtrada de efectos ex�genos
  ARIMADegRange,      //Rangos de grados ARIMA
  id.ur,              //Ranking de ra�ces unitarias
  arma.cfg);          //Configuraci�n

//////////////////////////////////////////////////////////////////////////////
//Identificador de la estructura ARIMA, estimaci�n y diagnosis
//////////////////////////////////////////////////////////////////////////////
//Configuraci�n por defecto
NameBlock arima.cfg = ArimaTools::ARIMA.Identifier::@Config::Default(?);
//Adaptaci�n de la configuraci�n al caso particular
//Los modelos de este ejemplo son peque�os y r�pidos de estimar por lo que 
//no hay porqu� poner un l�mite de intentos fallidos.
Real arima.cfg::maxTry := 1/0;
//Si al llegar a 20 intentos ya hay uno bueno paramos
Real arima.cfg::maxTryForGood := 20;
//Si al llegar a 50 intentos ya hay uno aceptable paramos
Real arima.cfg::maxTryForAcceptable := 50;
//Identificador de la estructura ARIMA que recorre el ranking propuesto
//estimando y diagnosticando cada modelo hasta que se cumplen las
//condiciones m�nimas exigidas o se aborta el proceso y devuelve el mejor 
//modelo encontrado hasta ese momento.
Set id.arima = ArimaTools::ARIMA.Identifier::Identify(
// output, id.arma::ranking, (filter::_.model::Definition)->Input, Mercadona::Config::arima.cfg);
  output,             
  id.arma::ranking, 
  filter::_.model::Definition->Input, 
  arima.cfg);
//Si s�lo hay rechazados descartamos los que dan problemas de estacionariedad
Set id.arima.stationary = Select(id.arima,Real(Set m)
{
  @TestResult stat = m::modEst::Diagnostics::ParamStationary;
  stat->Punctuation <stat->Refuse
});

/* */
