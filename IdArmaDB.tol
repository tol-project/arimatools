//////////////////////////////////////////////////////////////////////////////
Class @IdArmaDB
//////////////////////////////////////////////////////////////////////////////
{
  Static Real   _.default.lag = 20;
  Static Matrix _.default.weights = Col(1,1,1,1,1,1);

  Real _.n_min;
  Real _.n_max;
  Real _.lag;

  Matrix _.p.weights;
  Matrix _.q.weights;
  Real _.p.max = ?;
  Real _.q.max = ?;

  Matrix _p.sample = Constant(0,0,?);
  Matrix _q.sample = Constant(0,0,?);

  Static Text _.dbEngine = "";
  Static Set _.dbConnection = Copy(Empty);
  Static Text _.table_sufix = "";

  ////////////////////////////////////////////////////////////////////////////
  Static NameBlock DefaultConnection(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    DBConnect::Create (
      "bysforidarma","","", 
      "ODBC", "bysforidarma", "", 
      "Identification or ARMA models")
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Real HasConnection(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Card(_.dbConnection)>0
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Real Open(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set If(!Card(@IdArmaDB::_.dbConnection),
    {
      Text @IdArmaDB::_.dbEngine := "sqlite";
      Set @IdArmaDB::_.dbConnection := 
       @NameBlock(@IdArmaDB::DefaultConnection(?))
    });
    Real ok = $(@IdArmaDB::_.dbConnection)::Open(?);
    Real Case(
//SELECT load_extension('extension-function.dll');
    @IdArmaDB::_.dbEngine == "sqlite",
    {
      Real DBExecQuery("SELECT load_extension('"+
        If(OSWin,"extension-function.dll","libsqlitefunctions.so")+"');");
      True
    },
    @IdArmaDB::_.dbEngine == "postgresql",
    {
      Real If(OSWin,DBExecQuery("set client_encoding to 'LATIN1'"));
      True
    });
    ok
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Real Close(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    $(@IdArmaDB::_.dbConnection)::Close(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real initialize(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.p.max := Rows(_.p.weights)-1;
    Real _.q.max := Rows(_.q.weights)-1;
    Matrix p.w = Round(_.p.weights*1/MatMin(_.p.weights));
    Matrix q.w = Round(_.q.weights*1/MatMin(_.q.weights));
    Matrix _p.sample := Group("ConcatRows",
      For(0,_.p.max,Matrix(Real k)
      {
        Constant(MatDat(p.w,k+1,1),1,k)
      }));
    Matrix _q.sample := Group("ConcatRows",
      For(0,_.q.max,Matrix(Real k)
      {
        Constant(MatDat(q.w,k+1,1),1,k)
      }));
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @IdArmaDB Create(
    Real n_min, 
    Real n_max, 
    Matrix p.weights, 
    Matrix q.weights,
    Real lag)
  ////////////////////////////////////////////////////////////////////////////
  {
    @IdArmaDB aux = [[
      Real _.n_min = n_min;
      Real _.n_max = n_max;
      Real _.lag = lag;
      Matrix _.p.weights = p.weights;
      Matrix _.q.weights = q.weights
    ]];
    Real aux::initialize(?);
    aux
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @IdArmaDB Symmetric(
    Real n_min, 
    Real n_max, 
    Matrix weights,
    Real lag)
  ////////////////////////////////////////////////////////////////////////////
  {
    @IdArmaDB::Create(n_min,n_max,weights,weights,lag)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @IdArmaDB Default(Real n_min, Real n_max)
  ////////////////////////////////////////////////////////////////////////////
  {
    @IdArmaDB::Symmetric(
      n_min, 
      n_max, 
      @IdArmaDB::_.default.weights,
      @IdArmaDB::_.default.lag)
  };
      
  ////////////////////////////////////////////////////////////////////////////
  Text ModTabSuf(Text suf)
  ////////////////////////////////////////////////////////////////////////////
  {
    "model_f_acf_"<<_.lag+suf+"_"<<_.n_min+"_"<<_.n_max
  };

  ////////////////////////////////////////////////////////////////////////////
  Text ModTabNam(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    ModTabSuf("")
  };

  ////////////////////////////////////////////////////////////////////////////
  Real DropTables(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real @IdArmaDB::Open(?);
    Text table = ModTabNam(?);
    Real DBExecQuery(
    "DROP TABLE IF EXISTS "+table+";\n"
    "DROP TABLE IF EXISTS "+table+"_stats;\n");
    Real @IdArmaDB::Close(?);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real CreateTables(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real @IdArmaDB::Open(?);
    Text ifNotExists = Case(
     @IdArmaDB::_.dbEngine=="sqlite","IF NOT EXISTS",
     @IdArmaDB::_.dbEngine=="postgresql","");
    Text singlePrecision = Case(
     @IdArmaDB::_.dbEngine=="sqlite","float",
     @IdArmaDB::_.dbEngine=="postgresql","real");
    Text table = ModTabNam(?);
    Text query = 
    "CREATE TABLE "+ifNotExists+" "+table+" \n"
    "( \n"
    "  id_model integer NOT NULL, \n"
    "  data smallint NOT NULL, \n"
    "  nu_lag smallint NOT NULL, \n"
    "  nu_p smallint NOT NULL, \n"
    "  nu_q smallint NOT NULL, \n"
    "  te_ar text NOT NULL, \n"
    "  te_ma text NOT NULL, \n"+
    "  acf_1_int1 smallint NOT NULL, \n"
    "  acf_2_int1 smallint NOT NULL, \n"
    "  pacf_2_int1 smallint NOT NULL, \n"+
    SetSum(For(1,_.lag,Text(Real k){
    "  acf_"<<k+" "+singlePrecision+" NOT NULL, \n"
    }))+
    SetSum(For(2,_.lag,Text(Real k){
    "  pacf_"<<k+" "+singlePrecision+" NOT NULL, \n"
    }))+
    "  CONSTRAINT pk_"+table+" PRIMARY KEY (id_model ) \n"
    "); \n";
    WriteLn("[@IdArmaDB::CreateTables] query= {\n"+query+"\n}\n");
    Real DBExecQuery(query);
    Real @IdArmaDB::Close(?);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real DbReadOnlyMode(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real Case(
    @IdArmaDB::_.dbEngine == "sqlite",
    {
      Real DBExecQuery("PRAGMA synchronous = ON");
      Real DBExecQuery("PRAGMA journal_mode = DELETE");
      Real DBExecQuery("PRAGMA locking_mode = NORMAL");
      True
    },
    1==1, True);

    Text ifNotExists = Case(
     @IdArmaDB::_.dbEngine=="sqlite","IF NOT EXISTS",
     @IdArmaDB::_.dbEngine=="postgresql","");
    Text table = ModTabNam(?);

    Real DBExecQuery(
    "CREATE INDEX  "+ifNotExists+" idx_"+table+"_p_q_data_acf_1_acf_2_pacf_2 "+
    "ON "+table+" (nu_p, nu_q, data, acf_1_int1 , acf_2_int1 , pacf_2_int1 );");

/*
    Real DBExecQuery(
    "CREATE INDEX "+ifNotExists+" gist_"+table+"_acf_1_acf_2 "
    "ON "+table+" USING gist (box(point(acf_1,acf_2),point(acf_1,acf_2)) );\n"
    "CREATE INDEX "+ifNotExists+" gist_"+table+"_acf_1_pacf_2 "
    "ON "+table+" USING gist (box(point(acf_1,pacf_2),point(acf_1,pacf_2)) );\n");
*/
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real DbWriteOnlyMode(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real Case(
    @IdArmaDB::_.dbEngine == "sqlite",
    {
      Real DBExecQuery("PRAGMA synchronous = OFF");
      Real DBExecQuery("PRAGMA journal_mode = OFF");
      Real DBExecQuery("PRAGMA locking_mode = EXCLUSIVE");
      True
    },
    1==1, True);
    Text table = ModTabNam(?);
    Real DBExecQuery(
    "DROP INDEX IF EXISTS idx_"+table+"_p_q_data_acf_1_acf_2_pacf_2;\n"
    );
/*
    Real DBExecQuery(
    "DROP INDEX IF EXISTS gist_"+table+"_acf_1_acf_2;\n"
    "DROP INDEX IF EXISTS gist_"+table+"_acf_1_pacf_2;\n"
    );
*/
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real DbCreateStatsTable(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    WriteLn("[@IdArmaDB] Creating statistics table ...");
    Text table = ModTabNam(?);
    Text singlePrecision = Case(
     @IdArmaDB::_.dbEngine=="sqlite","float",
     @IdArmaDB::_.dbEngine=="postgresql","real");


    Text query = 
    "CLUSTER "+ModTabNam(?)+
    " USING idx_"+ModTabNam(?)+"_p_q_data_acf_1_acf_2_pacf_2;\n"+
    "DROP TABLE IF EXISTS "+table+"_stats;\n"
    "CREATE TABLE "+table+"_stats \n"
    "( \n"
    "  nu_sample_size integer NOT NULL, \n"
    "  n_min smallint NOT NULL, \n"
    "  n_max smallint NOT NULL, \n"
    "  nu_p_min smallint NOT NULL, \n"
    "  nu_p_max smallint NOT NULL, \n"
    "  nu_q_min smallint NOT NULL, \n"
    "  nu_q_max smallint NOT NULL"+
    SetSum(For(1,_.lag,Text(Real k){
    ", \n  acf_"<<k+"_min "+singlePrecision+" NOT NULL"
    ", \n  acf_"<<k+"_max "+singlePrecision+" NOT NULL"
    }))+
    SetSum(For(2,_.lag,Text(Real k){
    ", \n  pacf_"<<k+"_min "+singlePrecision+" NOT NULL"
    ", \n  pacf_"<<k+"_max "+singlePrecision+" NOT NULL"
    }))+"\n"
    "); \n"
    "INSERT INTO "+table+"_stats \n"
    "SELECT  \n"
    "  count(*) as nu_sample_size,  \n"
    "  min(data) as n_min,  \n"
    "  max(data) as n_max,  \n"
    "  min(nu_p) as nu_p_min,  \n"
    "  max(nu_p) as nu_p_max,  \n"
    "  min(nu_q) as nu_q_min,  \n"
    "  max(nu_q) as nu_q_max"+
    SetSum(For(1,_.lag,Text(Real k){
    ", \n  min(acf_"<<k+") as acf_"<<k+"_min"
    ", \n  max(acf_"<<k+") as acf_"<<k+"_max"
    }))+
    SetSum(For(2,_.lag,Text(Real k){
    ", \n  min(pacf_"<<k+") as pacf_"<<k+"_min"
    ", \n  max(pacf_"<<k+") as pacf_"<<k+"_max"
    }))+"\n"
    "FROM "+table+";";
    WriteLn(query);
    Real DBExecQuery(query)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real MergeTables(Set sufixes)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real @IdArmaDB::Open(?);
    Real DbWriteOnlyMode(?);
    Text tab_0 = ModTabSuf("");
    Real n = Card(sufixes);
    Set tab_k = For(1,n,Text(Real k) 
    { 
      ModTabSuf(sufixes[k])
    });
    Real cum = DBTable("select count(*) from "+tab_0)[1][1];
    WriteLn("Current registers at "+tab_0+" : "<<cum);
    Set cum_k = For(1,n,Real(Real k) 
    { 
      Real c = Copy(cum);
      Real reg = DBTable("select count(*) from "+tab_k[k])[1][1];
      WriteLn("Current registers at "+tab_k[k]+" : "<<reg);
      Real cum := cum + reg;
      c
    });
    Text no_id_fields = "data, nu_lag, nu_p, nu_q, te_ar, te_ma"
      ",acf_1_int1, acf_2_int1, pacf_2_int1"
      <<SetSum(For(1,_.lag,Text(Real k) { ", acf_"<<k }))
      <<SetSum(For(2,_.lag,Text(Real k) { ", pacf_"<<k }));
    Set For(1,n,Real(Real k)
    {
      Text query = "INSERT INTO "+tab_0+" "+
      "(id_model, "+no_id_fields+")\n"+
      "SELECT id_model+"<<cum_k[k]+ " as id_model, "+no_id_fields+"\n"
      "FROM "+tab_k[k]+";\n";
      WriteLn(query);
      Real DBExecQuery(query);
      True
   });
   Real DbReadOnlyMode(?);
   Real DbCreateStatsTable(?);
   Real @IdArmaDB::Close(?);
   True 
  };

  ////////////////////////////////////////////////////////////////////////////
  Real rand.p(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    MatDat(_p.sample,IntRand(1,Rows(_p.sample)),1)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real rand.q(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    MatDat(_q.sample,IntRand(1,Rows(_q.sample)),1)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set DrawOne(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real p = rand.p(?);
    Real q = rand.q(?);
    NameBlock model = ArimaTools::@RandArimaSeries::Create.ARMARegular(
      p,q,1, IntRand(_.n_min,_.n_max), C, Today);
    Serie noise = MatSerSet(Tra(model::noise),C,Today)[1];
    Matrix acf = Sub(AutoCor(noise,_.lag),1,2,_.lag,1);
    Matrix pacf = Sub(PartAutoCor(noise,_.lag),1,2,_.lag,1);
    Real N = CountS(noise);
    Set aux = [[N,p,q,model::ar,model::ma,acf,pacf]];
    Real SetIndexByName(aux);
    aux
  };

  ////////////////////////////////////////////////////////////////////////////
  Real DrawAndStore(Real sampleLength, Real last_id)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real t0 = Copy(Time);
    Real PutRandomSeed(0);
    Set sample = For(1,sampleLength,Set(Real iter)
    {
      If(!(iter%100),WriteLn("[@IdArmaDB] "
        "Building simulation "<<iter+" of "<<sampleLength));
      DrawOne(?)
    });
    Real t1 = Copy(Time);
    Real tc = t1-t0;
    WriteLn("[@IdArmaDB] "
      "Created "<<sampleLength+" models in "<<FormatReal(tc,"%.2lf")+" s.");
    Real DBExecQuery("BEGIN TRANSACTION");
    WriteLn("[@IdArmaDB] Storing "<<sampleLength+" models ...");
    Text table = ModTabNam(?);

    Set For(1,sampleLength,Real(Real iter)
    {
      Set s = sample[iter];
      Real id_model = last_id+iter;

      Text insert.model = "INSERT INTO "+table+" "+
      "(id_model, data, nu_lag, nu_p, nu_q, te_ar, te_ma"
      ",acf_1_int1,acf_2_int1,pacf_2_int1"
      <<SetSum(For(1,_.lag,Text(Real k) { ", acf_"<<k }))
      <<SetSum(For(2,_.lag,Text(Real k) { ", pacf_"<<k }))+") VALUES "+
      "("<<id_model+","<<s::N+","
         <<_.lag+","<<s::p+","<<s::q
         <<",'"<<s::ar+"','"<<s::ma+"'"
         <<","<<Real Round(MatDat(s::acf,1,1)*100) 
         <<","<<Real Round(MatDat(s::acf,2,1)*100) 
         <<","<<Real Round(MatDat(s::pacf,2,1)*100) 
         <<SetSum(For(1,_.lag,Text(Real k) { ","<<FormatReal(MatDat(s::acf,  k, 1),"%.7lf") }))
         <<SetSum(For(2,_.lag,Text(Real k) { ","<<FormatReal(MatDat(s::pacf, k, 1),"%.7lf") }))+
       ")";
       Real DBExecQuery(insert.model)
    });
    Real DBExecQuery("COMMIT TRANSACTION");
    Real ti = Copy(Time)-t1;
    WriteLn("[@IdArmaDB] "
      "Inserted "<<sampleLength+" models in "<<FormatReal(ti,"%.2lf")+" s.");
    True  
  };

  ////////////////////////////////////////////////////////////////////////////
  Real CycleDrawAndStore(Real sampleLength, Real times)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real t0 = Copy(Time);
    Real @IdArmaDB::Open(?);
    Text table = ModTabNam(?);
    Real last_id = DBTable("select count(*) from "+table)[1][1];
    Real iterPack = 1;
    Real DbWriteOnlyMode(?);
    While(iterPack<=times,
    {
      Real elapsed = Copy(Time)-t0;
      Real remaining = elapsed*(times-iterPack+1)/(iterPack-1);
      
      WriteLn("[@IdArmaDB] "
        "Building simulation pack "<<iterPack+" of "<<times+
        If(iterPack==1,""," elapsed:"<<FormatReal(elapsed,"%.2lf")+
        " s. remaining:"<<FormatReal(remaining,"%.2lf")+" s."));
      Real DrawAndStore(sampleLength, last_id);
      Real last_id := last_id + sampleLength;
      Real iterPack := iterPack+1
    });
    Real DbReadOnlyMode(?);
    Real DbCreateStatsTable(?);
    Real @IdArmaDB::Close(?);
    Real tm = Copy(Time)-t0;
    WriteLn("[@IdArmaDB] "
    "Inserted and indexed "<<Real(sampleLength*times)+
    " models in "<<FormatReal(tm,"%.2lf")+" s.");
    True
  }

  
};



